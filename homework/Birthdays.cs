﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace homework
{
    public partial class Birthdays : Form
    {
        private string username;
        public Birthdays(string user)
        {
            InitializeComponent();
            username = user;
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string name;
            string date;
            string globalDate;
            string day;
            string month;
            string year;
            int comaIndex;
            int firstSlashIndex;
            int lastSlashIndex;
            char digit;
            bool birthday = false;

            if (!System.IO.File.Exists(username + "BD.txt"))
            {
                var myFile = System.IO.File.Create(username + "BD.txt");
                myFile.Close();
            }
            else
            {
                string[] lines = System.IO.File.ReadAllLines(username + "BD.txt");

                foreach(string line in lines)
                {
                    comaIndex = line.IndexOf(','); //These 3 lines seperate each row from the file to name and birthday
                    name = line.Substring(0, comaIndex);
                    date = line.Substring(comaIndex + 1);

                    firstSlashIndex = date.IndexOf('/');
                    lastSlashIndex = date.LastIndexOf('/');

                    //Taking the date and splitting it to day, month and year
                    month = date.Substring(0, firstSlashIndex);
                    day = date.Substring(firstSlashIndex + 1, lastSlashIndex - firstSlashIndex - 1);
                    year = date.Substring(lastSlashIndex + 1);

                    globalDate = monthCalendar1.SelectionRange.Start.ToShortDateString(); //taking the date that the mouse clicked at
                    lastSlashIndex = globalDate.LastIndexOf('/');
                    globalDate = globalDate.Substring(0, lastSlashIndex);
                    date = "";
                    date = day + "/" + month; //Taking only the day and month

                    firstSlashIndex = globalDate.IndexOf('/');
                    day = globalDate.Substring(0, firstSlashIndex);
                    month = globalDate.Substring(firstSlashIndex + 1);

                    /*
                    * These 2 'if' sentences check if there is a 0 at the begining of the day or month that we don't want.
                    * For example: if the month is March then it will be shown as 03 and we want 3.
                    * Same as day
                    */

                    if(day[0] == '0') 
                    {
                        digit = day[1];
                        day = "";
                        day = day + digit;
                    }

                    if (month[0] == '0')
                    {
                        digit = month[1];
                        month = "";
                        month = month + digit;
                    }

                    globalDate = day + '/' + month;

                    if (date.Equals(globalDate)) //Checks if the person that we check now has a birthday.
                    {
                        label1.Text = name + " has a birthday today";
                        birthday = true;
                    }
                }

                if(!birthday)
                {
                    label1.Text = "No one has a birthday today";
                }
                
            }
                
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }
    }
}
