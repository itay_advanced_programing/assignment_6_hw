﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace homework
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] lines = System.IO.File.ReadAllLines("Users.txt");
            bool correct = false;
            int comaIndex;
            string username;
            string password;

            foreach(string line in lines)
            {
                comaIndex = line.IndexOf(',');
                username = line.Substring(0, comaIndex);
                password = line.Substring(comaIndex + 1);

                if((username.Equals(textBox1.Text)) && (password.Equals(textBox2.Text)))
                {
                    Form bd = new Birthdays(username);
                    this.Hide();
                    bd.ShowDialog();
                    correct = true;
                }
            }

            if(!correct)
            {
                MessageBox.Show("Username or password are incorrect","ERORR");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
